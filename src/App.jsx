// import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { Component } from "react";
import { Link, Outlet } from "react-router-dom";
import { Nav, Navbar, Container } from 'react-bootstrap';
// import { useAuthState } from 'react-firebase-hooks/auth';
// import { getAuth, signOut } from 'firebase/auth';
// import firebaseConfig from './config/firebaseConfig';
// import { useEffect } from 'react';
// import {useState} from 'react';

// import Homepage from "./components/Homepage";
// import Register from "./components/Register.jsx";
// import Dashboard from "./components/Dashboard.jsx";
// import Login from "./components/Login.jsx";
// import User from "./components/User.jsx";





function App() {
  
  // const auth = getAuth(firebaseConfig)
  // const [user, ] = useAuthState(auth)

  // useEffect(() => {
  //   console.log(user)
  // }, [user])
  


  return (
    <div className="App mb-5">
      <nav className="navbar navbar-expand-lg navbar-light" style={{ backgroundColor: '#adb5bd'}}>
        
        <div className="container-fluid">
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                    <Navbar.Brand as={Link} className="nav-link active" aria-current="page" to="/">KETAWA.IN</Navbar.Brand>
                </li>
            </ul>
            <ul className="navbar-nav me-2 mb-2 mb-lg-0">
                <li className="nav-item">
                    <Nav.Link as={Link} className="nav-link active" aria-current="page" to="/dashboard">D</Nav.Link>
                </li>
                <li className="nav-item">
                    <Nav.Link as={Link} className="nav-link active" aria-current="page" to="/user">U</Nav.Link>
                </li>
                <li className="nav-item">
                    <Nav.Link as={Link} className="nav-link active" aria-current="page" to="/register">Register</Nav.Link>
                </li>
                <li className="nav-item">
                    <Nav.Link as={Link} className="nav-link active" aria-current="page" to="/login">Log in</Nav.Link>
                </li>
            </ul>
            </div>
        </div>
        
      </nav>
      <Outlet />
    </div>
    // <BrowserRouter>
    //   <Routes>
    //       <Route index element={<Homepage />} />
    //       <Route path="/register" element={<Register />} />
    //       <Route path="/dashboard" element={<Dashboard />} />
    //       <Route path="/login" element={<Login />} />
    //       <Route path="/user" element={<User />} />
    //   </Routes>
    // </BrowserRouter>
  );
}

export default App;
