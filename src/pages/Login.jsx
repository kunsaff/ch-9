import React from 'react'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

const Login = () => {
  return (
    <Container className="mt-5">
      <Row>
        <Col style={{ backgroundColor: '#adb5bd', height: '60vh'}}>
          <h1 className="mb-4">Login</h1>
          <Form>
            <Form.Group className="mb-3 fs-5" controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" placeholder="Enter email address" />
            </Form.Group>

            <Form.Group className="mb-3 fs-5" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password" />
            </Form.Group>
            <Button className="mt-2 mb-5" variant="primary" type="submit">
              Login
            </Button>
          </Form>
        </Col>
        <Col style={{ backgroundColor: '#198754', height: '60vh' }}>konten</Col>
      </Row>
    </Container>
  )
}

export default Login
