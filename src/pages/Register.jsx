import React from 'react'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import {useState, useEffect} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useCreateUserWithEmailAndPassword } from 'react-firebase-hooks/auth'
import {getAuth} from 'firebase/auth'
import firebaseApp from '../config/firebaseConfig'
import { useNavigate } from 'react-router-dom'

const auth = getAuth(firebaseApp)

const Register = () => {
  const navigate = useNavigate()
  const [credentials, setCredentials] = useState({
    username: '',
    email: '',
    password: ''
  })

  const [
    createUserWithEmailAndPassword,
    user,,] = useCreateUserWithEmailAndPassword(auth);
  
  useEffect(() => {
        if(user !== undefined)
            navigate('/dashboard', { replace: true });
    }, [user, navigate])
  
  return (
    <Container className="mt-5">
      <Row>
      <Col style={{ backgroundColor: '#198754', height: '60vh' }}>konten</Col>
        <Col style={{ backgroundColor: '#adb5bd', height: '60vh'}}>
          <h1 className="mb-4">Register</h1>
          <Form>
            <Form.Group className="mb-3 fs-5" controlId="formBasicUsername">
              <Form.Label>Username</Form.Label>
              <Form.Control type="username" placeholder="Enter username" onChange={(e)=> setCredentials({...credentials, username: e.target.value})} className="form-control" value={credentials.username} />
            </Form.Group>
            <Form.Group className="mb-3 fs-5" controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" placeholder="Enter email address" onChange={(e)=> setCredentials({...credentials, email: e.target.value})} className="form-control" value={credentials.email} />
            </Form.Group>

            <Form.Group className="mb-3 fs-5" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password" onChange={(e)=> setCredentials({...credentials, password: e.target.value})} className="form-control" value={credentials.password} />
            </Form.Group>
            <Button className="mt-2 mb-5" variant="primary" type="submit" onClick={() => createUserWithEmailAndPassword(credentials.email, credentials.password)}>
              Register
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>

    // <div style={{ height: '100vh' }}>
    //   <div className="container mt-5">
    //     <div className="row">
    //       <div className="col border text-white d-flex justify-content-center align-items-center" style={{ backgroundColor: '#198754', height: '60vh'}}>
    //         <h2>
    //         konten
    //         </h2>
    //       </div>
    //       <div className="col border" style={{ backgroundColor: '#adb5bd', height: '60vh'}}>
    //         <div className="me-2 ms-2 mt-1">
    //           <h1 className="mb-4">Register</h1>
    //           <form>
    //             <div className="mb-1">
    //               <label htmlFor="username" className="form-label fs-5">Username</label>
    //               <input onChange={(e)=> setCredentials({...credentials, username: e.target.value})} type="username" className="form-control" id="username" value={credentials.username} />
    //             </div>
    //             <div className="mb-1">
    //               <label htmlFor="email" className="form-label fs-5">Email</label>
    //               <input onChange={(e)=> setCredentials({...credentials, email: e.target.value})} type="email" className="form-control" id="email" value={credentials.email} />
    //             </div>
    //             <div className="mb-1">
    //               <label htmlFor="password" className="form-label fs-5">Password</label>
    //               <input onChange={(e)=> setCredentials({...credentials, password: e.target.value})} type="password" className="form-control" id="password" value={credentials.password}/>
    //             </div>
    //             <div className="d-grid gap-2 d-md-flex justify-content-md-end mt-3">
    //               <button type="submit" className="btn btn-primary" 
    //               onClick={() => createUserWithEmailAndPassword(credentials.email, credentials.password)}>
    //               Submit
    //               </button>
    //             </div>
    //           </form>
    //           </div>    
    //       </div>
    //     </div>
    //   </div>

    // </div>
    
  )
}

export default Register