import firebaseApp from 'firebase/compat/app'
// Import the functions you need from the SDKs you need
// import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
export default firebaseApp.initializeApp(
    {
        apiKey: "AIzaSyAjBoJu2tS2sJbs5absHqrpvbHLDpCLPQU",
        authDomain: "ch-9-practice.firebaseapp.com",
        projectId: "ch-9-practice",
        storageBucket: "ch-9-practice.appspot.com",
        messagingSenderId: "194828293493",
        appId: "1:194828293493:web:8cf6490be13774778e2f3c",
        measurementId: "G-46WJCZ1D9E"
      }
)
// const firebaseConfig = 

// // Initialize Firebase
// const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);